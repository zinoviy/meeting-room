from django.urls import path

from .views import *


urlpatterns = [
    path('', index, name='main'),
    path('signup/', signup, name="signup"),
    path('login/', login, name="login"),
    path('logout/', logout, name="logout"),
    path('add-event/', add_event, name="add-event"),
    path('deactivate-event/', deactivate_event, name="deactivate-event"),
    path('statictics/', statistics, name="statistics"),
    path('meeting-room/', meeting_room, name="meeting-room"),
    path('meeting-room/<int:room_id>', meeting_room_id, name='meeting-room-id'),
]
