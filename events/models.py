from django.db import models
from django.urls import reverse


class MeetingRoom(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name
