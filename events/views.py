from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseNotFound

from events.models import MeetingRoom


def index(request):
    #return HttpResponse('Main page')
    context = {}
    return render(request, 'events/index.html', context=context)


def signup(request):
    return HttpResponse('Signup page')


def login(request):
    return HttpResponse('Login page')


def logout(request):
    return HttpResponse('Logout page')


def add_event(request):
    meeting_room = MeetingRoom.objects.last()
    counter = str(int(meeting_room.description) + 1)
    print(counter)
    meeting_room.description = counter
    meeting_room.save()
    return HttpResponse(f'<h1>Number of counter: {counter}</h1>')


def deactivate_event(request):
    return HttpResponse('Deactivate event page')


def statistics(request):
    return HttpResponse('Statictics page')


def meeting_room(request):
    return HttpResponse('Meeting room page')


def meeting_room_id(request, room_id):
    return HttpResponse(f'Meeting room #{room_id} page')


def pageNotFound(request, exception):
    return HttpResponseNotFound('<h1>Страница не найдена</h1>')
