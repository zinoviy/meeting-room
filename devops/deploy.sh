#!/usr/bin/env bash
ssh -o StrictHostKeyChecking=no root@157.245.21.112 << 'ENDSSH'
 cd /meeting-room
 docker login -u $REGISTRY_USER -p $CI_BUILD_TOKEN $CI_REGISTRY
 docker pull registry.gitlab.com/zinoviy/meeting-room:latest
 docker-compose up -d
ENDSSH
