from django.contrib import admin
from django.urls import path
from django.urls import include

from events.views import pageNotFound

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('events.urls')),
]

handler404 = pageNotFound
